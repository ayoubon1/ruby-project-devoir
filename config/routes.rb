Rails.application.routes.draw do
  resources :articles
  root "articles#home"
  get "/accueil", to: "articles#home"
  resources :profiles
end
